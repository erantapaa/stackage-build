This repo contains scripts useful for building all of Stackage.

Building all of Stack with Docs
===

This a a recipe for building all of Stack with docs using
a custom version of Haddock.

Let STACK_HOME, RESOLVER, ARCH and GHC_VERSION  be your stack home directory
(usually ~/.stack), resolver name (e.g. `lts-5.13`), arch setting
and GHC version respectively.

1. Build your customer `haddock` binary. Let's call it `haddock-real`
Install it in a place like `$HOME/bin`

2. Install the wrapper script `wrap-haddock` at a path like `~/bin/wrap-haddock`.
Call this location WRAP_HADDOCK.

3. Modify the script `STACK_HOME/programs/ARCH/GHC_VERSION/bin/haddock`
as follows:

    exedir="$HOME/bin"
    exeprog=haddock-real
    ...
    exec WRAP_HADDOCK "$executablename" -B"$topdir" -l"$topdir" ${1+"$@"}

The way `haddock` gets called from `stack` is as follows:

- `stack build` invokes `STACK_HOME/programs/ARCH/GHC_VERSION/bin/haddock`
- That script invokes WRAP_HADDOCK with the path to the real haddock binary as the
first argument.
- WRAP_HADDOCK logs the call, may alter the command line parameters and passes
control to the real haddock program.
- In some cases WRAP_HADDOCK may decide not to call the real haddock program,
e.g. if you want to leave rebuilding the Haddock index and content pages until
every module has been built.

Alternatively you can run:

    install-wrap-haddock --resolver RESOLVER --real-haddock REAL_HADDOCK --wrapper WRAP_HADDOCK

to perform these edits for you.

4. Run:

    stackage-build --res RESOLVER --all-packages > all
    stackage-build --res RESOLVER --pack all --build-missing

The first command fetches ths list of all package-versions for the
indicated resolver into the file `all`. The second command issues
`stack build ...` commands for any packages listed in `all` which
do not appear in the resolver's package database directory.

You can run the `--build-missing` command over and over again
until all packages are built. For instance, you may need to install
additional system components (C libraries, programs, etc.) to get
certain Haskell packages to build.

To build a specific list of packages, use:

    stackage-build --res RESOLVER --pack file --build

where `file` contains the list of package-versions to build.

5. When all packages have been built, you can run:

    stackage-build --res RESOLVER --gen-index

To re-create the Haddock index and table of contents pages.

