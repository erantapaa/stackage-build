#!/bin/bash
#
# $1 - target full name
# $2 - target base name
# $3 - target path

pkgname=$(basename "$2")

redo-ifchange deps.db ./lookup-plus
deps=$(./lookup-plus deps.db "$pkgname" "done/%.pkg")

redo-ifchange ${deps[@]}
echo building $2 >&2

resolver=$(cat RESOLVER)

echo -n 'START TIME: '
date
stack install --resolver "$resolver" "$pkgname"
echo -n 'END TIME: '
date

