#!/bin/sh

exec >&2
redo-ifchange all-packages RESOLVER
resolver=$(cat RESOLVER)
echo
echo Building $1 for resolver "$resolver"
cat all-packages | time parallel -j20 -m ./fetch-deps $3 "$resolver"
echo
echo Building reverse dependencies
time ./make-revdeps $3

