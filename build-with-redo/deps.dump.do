#!/usr/bin/env python
#
# emit the dependencies in pairs suitable for input to tsort

import subprocess
import sqlite3

def main():
  subprocess.check_call(['redo-ifchange', 'deps.db'])

  dbpath = "deps.db"
  conn = sqlite3.connect(dbpath)

  c = conn.cursor()
  for r in c.execute("SELECT package, deps FROM deps"):
    deps = r[1].split()
    for d in deps:
      print r[0], d

if __name__ == '__main__':
  main();

