import sqlite3
import sys

def run_sql(conn, *stmts):
  c = conn.cursor()
  for stmt in stmts:
    c.execute(stmt)
  conn.commit()

def execute_sql(conn, sql, vals):
  try:
    cur = conn.cursor()
    cur.execute(sql, vals)
    conn.commit()
  except sqlite3.Error, e:
    conn.rollback()
    sys.stderr.write("error: " + str(e) + "\n")

def select_exists(conn, sql, vals):
  row = select_first(conn, sql, vals)
  return row != None

def select_first(conn, sql, vals):
  c = conn.cursor()
  c.execute(sql, vals)
  return c.fetchone()

def marks(n):
  return ','.join( ["?"] * n )

def where_clause(d):
  cons = []
  vals = []
  for k in d:
    cons.append("{} = ?".format(k))
    vals.append(d[k])
  clause = ' AND '.join(cons)
  return (clause, vals)

def exec_insert(conn, table, *vals):
  stm = "INSERT INTO {} VALUES ({})".format(table, marks( len(vals) ) )
  execute_sql(conn, stm, vals)

def exec_replace(conn, table, *vals):
  stm = "REPLACE INTO {} VALUES ({})".format(table, marks( len(vals) ) )
  execute_sql(conn, stm, vals)

def exec_exists(conn, table, **assigns):
  clause, vals = where_clause(assigns)
  stm = "SELECT 1 FROM {} WHERE {}".format(table, clause)
  select_exists(conn, stm, vals)

