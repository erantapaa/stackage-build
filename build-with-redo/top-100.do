#!/bin/bash

# redo-ifchange deps.dump

deps=($(tsort < deps.dump | tail -100))

d1=("${deps[@]/#/done/}")
d2="${d1[@]/%/.pkg}"

redo-ifchange $d2

