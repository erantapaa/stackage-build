
"""
1467838306.66 hidapi-0.1.4: ended
1467829745.62 hmatrix-0.17.0.1: ended
1467838333.0 hmatrix-gsl-0.17.0.0: ended
1467838343.42 hmatrix-gsl-stats-0.4.1.3: ended
1467838355.34 pango-0.13.1.1: ended
"""

import re
import os
import sys

# may be used with both output.log and output.mtimes
def read_events(path):
  count = 0
  with open(path) as f:
    for x in f:
      m = re.match('(\d+\.\d+)\s+(\S+):\s*(\S+)\s*\Z', x)
      if m:
        yield (float( m.group(1) ), m.group(2), m.group(3))
      else:
        # print "not matched:", x,
        pass
      count += 1

def merge_events(paths):
  all_events = []
  for path in paths:
    all_events.extend( list(read_events(path)) )
  all_events.sort()
  return all_events

