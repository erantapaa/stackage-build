
import re
import requests

def package_name(pkgvers):
  m = re.match('(.*?)-(\d[\d\.]*)\Z', pkgvers)
  if m:
    return m.group(1)
  else:
    return pkgvers

def fetch_dependencies(pkgvers):
  """Return the names of dependent packages (w/o versions) for a package version."""
  url = "https://hackage.haskell.org/package/{}".format(pkgvers)
  r = requests.get(url)
  t = r.text
  m = re.search('<th>Dependencies</th>\s*<td>(.*?)</td>', t, re.I|re.M)
  if not m:
    return None
  dephtml = m.group(1)
  return [ x.group(1) for x in re.finditer('href="/package/([^/]*?)"', dephtml) ]

