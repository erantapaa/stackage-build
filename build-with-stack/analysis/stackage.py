#!/usr/bin/env python

import requests
import re
import sys
import os
import collections

PackageVersion = collections.namedtuple("PackageVersion", ["package", "version"])

def fetch_cabal_config(resolver):
  url = "https://www.stackage.org/" + resolver + "/cabal.config"
  r = requests.get(url)
  pkgvers = []
  for m in re.finditer("^(\s+|constraints:\s+)?(\S+)\s*==\s*(\d[\d\.]*)", r.text, re.M):
    pkgvers.append( PackageVersion( package = m.group(2), version = m.group(3) ) )
  return pkgvers

def fetch_dependencies(resolver, pkgname):
  url = "https://www.stackage.org/{}/package/{}".format(resolver, pkgname)
  r = requests.get(url)
  # look for <div class="dep-list">...</div>
  t = r.text
  m = re.search('<div class="dep-list">(.*?)</div>', t)
  if m:
    hrefs = []
    for x in re.finditer('href="(.*?)"', m.group(1)):
      hrefs.append( os.path.basename( x.group(1) ) )
    return hrefs
  else:
    return None

def fetch_resolver_packages(resolver):
  """Return a list of package-versions for a resolver."""
  url = "https://www.stackage.org/{}".format(resolver)
  r = requests.get(url)
  t = r.text
  pattern = '<a href="https://www.stackage.org/{}/package/(.*?)"'.format(resolver)
  return [ x.group(1) for x in re.finditer(pattern, t) ]

