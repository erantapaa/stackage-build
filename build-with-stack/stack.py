#!/usr/bin/env python
#
# Install a redirection script for haddock in a stack environment.

import sys
import os
import re
import collections
from shutil import copyfile

class Path:
  def __init__(self, path, leaf=None):
    self.fullpath = path
    if leaf is None:
      self.leaf = os.path.basename(path)
    else:
      self.leaf = leaf
  def join(self, leaf):
    return Path( os.path.join( self.fullpath, leaf ), leaf )
  def __str__(self):
    return self.leaf
  def __repr__(self):
    return self.__str__()

def path_listdir(dir):
  if os.isdir(dir):
    for x in os.listdir(dir):
      yield Path( os.path.join(dir, x), x )
  else:
    return

def all_subdirs(dir):
  for x in path_listdir(dir.fullpath):
    if os.path.isdir(x.fullpath):
      yield x

def subdir(dir, sdir):
  path = dir.join(sdir)
  if os.path.isdir(path.fullpath):
    yield path

# return all arch, resolver, ghc tuples for a stack home directory
def generic_arch_resolver_ghc(dir):
  start = Path(dir)
  for p1 in all_subdirs( start ):
    for p2 in all_subdirs( p1 ):
      for p3 in all_subdirs( p2 ):
        yield (p1, p2, p3)

# return all arch, resolver, ghc tuples for a specific resolver
def generic_arch_ghc_for_resolver(dir, resolver):
  start = Path( dir )
  for p1 in all_subdirs( start ):
    for p2 in subdir(p1, resolver):
      for p3 in all_subdirs(p2):
        yield (p1, p3)

def stack_snapshots_arch_resolver_ghc(stack_home):
  generic_arch_resolver_ghc( os.path.join(stack_home, "snapshots") )

def stack_snapshots_arch_ghc_for_resolver(stack_home, resolver):
  dir = os.path.join(stack_home, "snapshots")
  return generic_arch_ghc_for_resolver(dir, resolver)

def stack_programs_arch_ghc(stack_home):
  for p1 in subdirs( os.path.join( stack_home, "programs" ) ):
    for p2 in subdirs( p1 ):
      yield (p1, p2)

def stack_programs_arch_for_ghc(stack_home, ghc_version):
  start = Path( os.path.join(stack_home, "programs") )
  for p1 in all_subdirs( start ):
    for p2 in subdir(p1, ghc_version):
      yield (p1, p2)

def stack_install_arch_resolver_ghc(stack_work):
  return generic_arch_resolver_ghc( os.path.join(stack_work, "install") )

def unique_element(it):
  first = None
  for x in it:
    if first:
      return None
    else:
      first = x
  return first

def find_ghc_directory(stack_home, ghc_version):
  programs = os.path.join(stack_home, "programs")
  return unique_element( stack_programs_arch_for_ghc(stack_home, ghc_version)) \
           or die ("unable to find the unique ghc directory for " \
                      + ghc_version + " under " + programs)


def is_exe(path, name):
  if not os.path.exists(path):
    die("{} does not exist: {}".format(name, path))
  if not os.path.isfile(path):
    die("{} is not a file: {}".format(name, path))
  if not os.access(path, os.X_OK):
    die("{} is not executable: {}".format(name, path))


def check_executable(path):
  is_exe(path, os.path.basename(path))


