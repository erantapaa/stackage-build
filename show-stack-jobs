#!/usr/bin/env python
#
# Emit a list of a pids associated with a redo process.
# This includes children of children.

import os
import sys
import re
import subprocess
import collections
import time

class ProcessTable():
  def __init__(self, procs):
    by_pid = {}
    parent_pid = {}
    child_pids = collections.defaultdict(set)

    for p in procs:
      pid = p['PID']
      ppid = p['PPID']
      by_pid[ pid ] = p
      parent_pid[ pid ] = ppid
      child_pids[ ppid ].add(pid)

    self.by_pid = by_pid
    self.parent_pid = parent_pid
    self.child_pids = child_pids

  def all_pids(self):
    return self.by_pid.keys()

  def ppid(self, pid):
    return self.parent_pid.get(pid, None)

  def children(self, pid):
    if pid in self.child_pids:
      return self.child_pids[pid]
    else:
      return set()

  def attr(self, key, pid):
    proc = self.by_pid.get(pid, None)
    if proc:
      return proc.get(key, None)
    return None

  def command(self, pid):
    return self.attr('COMMAND', pid)

  def root(self, pid):
    """Return the root of the process tree for this pid."""
    seen = set()
    while pid not in seen:
      seen.add(pid)
      ppid = self.ppid(pid)
      if ppid is None: return pid
      pid = ppid
    return None

def ptable_roots(ptable):
  allpids = set( ptable.all_pids() )
  roots = []
  for pid in allpids:
    ppid = ptable.ppid( pid )
    if ppid not in allpids:
      roots.append(pid)
  return roots

def parse_ps_output(text):
  lines = text.split('\n')
  header = lines[0]
  colnames = header.split()
  ncols = len(colnames)
  procs = []
  # print "ncols:", ncols, "colnames:", colnames

  for x in lines[1:]:
    cols = x.split(None, ncols-1)
    p = {}
    for i,v in enumerate(cols):
      if i >= ncols: print "bad cols:", cols
      p[ colnames[i] ] = v
      procs.append(p)
  return procs

def is_redo_proc(cmd):
  return re.match('\S+/python\s+\S+/redo[^/]*', cmd)

def shorten_command(cmd):
  words = cmd.split()
  w0 = os.path.basename(words[0])
  if (w0 == 'python' or w0 == 'bash' or w0 == 'sh'):
    words.pop(0)
    w0 = os.path.basename(words[0])
  elif w0 == 'setup-Simple-Cabal-1.22.5.0-ghc-7.10.3':
    w0 = 'setup'

  maxwidth = 40
  if w0 == 'redo-ifchange':
    maxwidth = 0

  # take arguments until length > 30
  args = ""
  i = 1
  while i < len(words):
    if len(args) + len(words[i]) > maxwidth: break
    args += " " + words[i]
    i = i + 1
  else:
    i = len(words)
  if i < len(words):
    args += " ..."

  return w0 + args

def process_cwd(pid):
  path = "/proc/{}/cwd".format(pid)
  try:
    return os.readlink(path)
  except:
    return "(???)"

def emit_tree(level, pid, ptable, seen):
  cmd = ptable.command(pid) or "(NONE)"
  cwd = process_cwd(pid)
  print "  " * level, "+", pid, shorten_command(cmd), cwd
  seen.add(pid)
  for cpid in ptable.children(pid):
    emit_tree(level+1, cpid, ptable, seen)

def emit_trees(ptable):
  roots = ptable_roots(ptable)
  seen = set()
  for pid in roots:
    emit_tree(0, pid, ptable, seen)

def is_stack_command(cmd, pid):
  if cmd is None:
    return False
  words = cmd.split()
  w0 = os.path.basename(words[0])
  return w0 == "stack"

def top_level_satisfying(ptable, predicate, seen, pid):
  """pid is a pid satifying the predicate"""
  tpid = pid
  seen.add(pid)
  while True:
    pid = ptable.ppid(pid)
    if not pid:
      return tpid
    if pid in seen:
      return None
    seen.add(pid)
    if predicate( ptable.command(pid), pid ):
      tpid = pid

def find_top_satisfying(ptable, predicate):
  top_pids = []
  seen = set()
  for pid in ptable.all_pids():
    if pid in seen: continue
    cmd = ptable.command(pid)
    if predicate(cmd, pid):
      tpid = top_level_satisfying(ptable, predicate, seen, pid)
      if tpid:
        top_pids.append(tpid)
  return top_pids

def find_top_stack(ptable):
  return find_top_satisfying(ptable, is_stack_command)

def is_redo_command(cmd, pid):
  if cmd is None:
    # print "cmd is None for pid", pid
    return False
  words = cmd.split()
  w0 = os.path.basename(words[0])
  if w0.startswith('redo'):
    return True
  if len(words) > 1:
    w1 = os.path.basename(words[1])
    return w1.startswith("redo")
  return False

def top_level_redo(pid, ptable, seen):
  tpid = pid
  seen.add(pid)
  while True:
    pid = ptable.ppid(pid)
    if not pid:
      return tpid
    if pid in seen:
      return None
    seen.add(pid)
    if is_redo_command( ptable.command(pid), pid ):
      tpid = pid

def find_top_redos(ptable):
  top_level_redos = []
  seen = set()
  for pid in ptable.all_pids():
    if pid in seen: continue
    cmd = ptable.command(pid)
    if is_redo_command(cmd, pid):
      tpid = top_level_redo(pid, ptable, seen)
      if tpid:
        top_level_redos.append(tpid)
  return top_level_redos

def tree_pids(pid, ptable, pids):
  pids.append(pid)
  for cpid in ptable.children(pid):
    tree_pids(cpid, ptable, pids)

def main():
  ps_output = subprocess.Popen(['ps', 'axo', 'pid,ppid,command'], stdout=subprocess.PIPE).communicate()[0]
  procs = parse_ps_output(ps_output)
  ptable = ProcessTable(procs)

  top_redos = find_top_redos(ptable)
  # print top_redos
  for pid in top_redos:
    pids = []
    tree_pids(pid, ptable, pids)
    print ' '.join(pids)

def show_cwd(ptable, toppid):
  seen = set()
  emit_tree(0, toppid, ptable, seen)

def collect_packages(ptable, pid, pkgs):
  cwd = process_cwd(pid)
  m = re.match('/tmp/stack\d+/(.*)', cwd)
  if m:
    pkgs.add(m.group(1))
  for cpid in ptable.children(pid):
    collect_packages(ptable, cpid, pkgs)

def test2():
  now = time.time()
  ps_output = subprocess.Popen(['ps', 'axo', 'pid,ppid,command'], stdout=subprocess.PIPE).communicate()[0]
  procs = parse_ps_output(ps_output)
  ptable = ProcessTable(procs)

  # find the stack --install job
  found = set()
  for p in procs:
    if 'stack install' in p['COMMAND']:
      found.add( p['PID'] )
  if len(found) != 1:
    print now, '???', found
  else:
    pid = list(found)[0]
    pkgs = set()
    collect_packages(ptable, pid, pkgs)
    print now, len(pkgs), ' '.join(pkgs)
  # show_cwd(ptable, '21233')

def test():
  ps_output = subprocess.Popen(['ps', 'axo', 'pid,ppid,command'], stdout=subprocess.PIPE).communicate()[0]
  procs = parse_ps_output(ps_output)
  ptable = ProcessTable(procs)

  show_cwd(ptable, '21233')
  return

  top_pids = find_top_stack(ptable)
  print top_pids
  for pid in top_pids:
    pids = []
    tree_pids(pid, ptable, pids)
    print ' '.join(pids)
    # print "tree pids for", pid, "=", pids

if __name__ == '__main__':
  test2()

