import re
import os
import itertools
import subprocess
import requests
import codecs
import fileinput

class StackResolver:
  def __init__(self, stack_home, resolver, arch, ghc_version):
    self.stack_home = stack_home
    self.resolver = resolver
    self.arch = arch
    self.ghc_version = ghc_version

  def haddock_path(self):
    return haddock_path(self.stack_home, self.arch, self.ghc_version)

  def docdir_path(self):
    return docdir_path(self.stack_home, self.arch, self.resolver, self.ghc_version)

  def gen_index_args(self):
    return gen_index_args(self.docdir_path())

  def user_package_db(self):
    return os.path.join( self.stack_home, "snapshots", self.arch, self.resolver, self.ghc_version, "pkgdb" )

  def list_installed_packages(self):
    """Return an iterable of the user installed packages."""
    pkgdb = self.user_package_db()
    output = subprocess.check_output(["stack", "--resolver", self.resolver, "exec", "ghc-pkg", "--", "list", "--package-db="+pkgdb])
    for x in output.split("\n"):
      m = re.match("^\s+([\w.-]+)", x)
      if m:
        yield m.group(1)

def find_resolver(resolver, stack_home = None):
  """Return a StackResolver object based on just a resolver name."""
  if stack_home is None:
    stack_home = os.path.join( os.path.expanduser("~") , ".stack" )
  pairs = list( itertools.islice(arch_ghc_version_pairs(stack_home, resolver), 0, 2) )
  if len(pairs) == 1:
    first = pairs[0]
    return StackResolver(stack_home, resolver, first[0], first[1])
  elif len(pairs) > 1:
    raise Exception("Multiple arch/ghc-versions match for resolver "+resolver)
  else:
    raise Exception("No matches for resolver " + resolver)

def find_dot_haddock(docdir, leaf):
  """Return the path of a .haddock file in docdir/leaf
     or None if one doesn't exist."""
  parts = leaf.split('-')
  for n in xrange(len(parts)-1, 0, -1):
    haddock = "-".join( parts[0:n] ) + ".haddock"
    if os.path.exists( os.path.join(docdir, leaf, haddock) ):
      return os.path.join(leaf, haddock)
  return None

def gen_index_args(docdir):
  """Return the -i arguments needed to rebuild the haddock index."""
  for leaf in os.listdir(docdir):
    if not os.path.isdir( os.path.join(docdir, leaf) ):
      continue
    dot_haddock = find_dot_haddock(docdir, leaf)
    if dot_haddock:
      yield "-i ./{0},./{1}".format(leaf, dot_haddock)

def arch_ghc_version_pairs(stack_home, resolver):
  """Return possible arch, ghc_version pairs for a resolver."""
  snapshots = os.path.join(stack_home, "snapshots")
  for arch in os.listdir(snapshots):
    rpath = os.path.join(snapshots, arch, resolver)
    if os.path.exists( rpath ):
      for ghc_version in list_ghc_versions( rpath ):
        yield (arch, ghc_version)

def list_ghc_versions(dir):
  # return the GHC versions in an resolver/arch directory
  for ent in os.listdir(dir):
    epath = os.path.join(dir, ent)
    if is_ghc_directory(epath):
      yield ent

def is_ghc_directory(path):
  # Return True if path is a directory and has certain subdirs
  if not os.path.isdir( path ):
    return False
  for subdir in ["doc", "lib", "pkgdb"]:
    if not os.path.isdir( os.path.join(path, subdir) ):
      return False
  return True

def docdir_path(stack_home, arch, resolver, ghc_version):
  """Return the doc directory path."""
  return os.path.join(stack_home, "snapshots", arch, resolver, ghc_version, "doc")

def haddock_path(stack_home, arch, ghc_version):
  """Return the haddock program path."""
  return os.path.join(stack_home, "programs", arch, "ghc-"+ghc_version, "bin", "haddock")

def get_stackage_cabal_config(resolver):
  """Return the cabal.config for a resolver."""
  url = "https://www.stackage.org/" + resolver + "/cabal.config"
  r = requests.get(url)
  if r.status_code == 200:
    return r.text

def cabal_packages(cabal_config):
  """Extract just the package names from a cabal.config."""
  for x in cabal_config.split("\n"):
    m = re.match("(?:constraints:)?\s+([\w-]+)\s+==", x)
    if m:
      yield m.group(1)

def cabal_package_versions(cabal_config):
  """Extract package, version pairs from a cabal.config."""
  for x in cabal_config.split("\n"):
    m = re.match("(?:constraints:)?\s+([\w-]+)\s+==([\w.]+)", x)
    if m:
      yield m.group(1), m.group(2)

def strip_version(pkg_vers):
  """Strip the version from a package-version string."""
  return re.sub("-[\d.]+$", "", pkg_vers)

def slurp_file(path):
  """Return the contents of a file."""
  with open(path) as f:
    return f.read()

def slurp_lines(path):
  """Return the lines in a file; interpret '-' as stdin."""
  if path == "-":
    return [ line.rstrip("\n") for line in sys.stdin.readlines() ]
  else:
    with open(path) as f:
      return [ line.rstrip("\n") for line in f.readlines() ]

def write_file(path, content):
  with codecs.open(path,'w') as f: f.write(content)

# from __future__ import with_statement # needed for 2.5 <= Python < 2.6
import contextlib, os

@contextlib.contextmanager
def remember_cwd():
    curdir = os.getcwd()
    try: yield
    finally: os.chdir(curdir)

